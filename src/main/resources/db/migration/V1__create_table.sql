   create table clientes (
        id bigint not null auto_increment,
        apellido varchar(255),
        email varchar(255),
        fecha_nacimiento date not null,
        nombre varchar(255),
        primary key (id)
    );