package com.example.authentication.controllers;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.authentication.dto.ClienteDTO;
import com.example.authentication.services.IClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

	@Autowired
	private IClienteService clienteService;

	@GetMapping(value = { "/listar" })
	public ResponseEntity<Object> listar() {
		List<ClienteDTO> clientes = this.clienteService.findAll();
		return new ResponseEntity<Object>(clientes, HttpStatus.OK);
	}
	
	@GetMapping(value = { "/consultar/{id}" })
	public ResponseEntity<Object> consultarCliente(@PathVariable("id") long id) {
		ResponseEntity<Object> response = null;
		Optional<ClienteDTO> cliente = this.clienteService.findOne(id);
		if (!cliente.isPresent()) {
			response = ResponseEntity.notFound().build();
		} else {
			response = new ResponseEntity<Object>(cliente, HttpStatus.OK);
		}
		return response;
	}

	@PutMapping(value = "/editar/{id}")
	public ResponseEntity<Object> editar(@PathVariable("id") long id, @RequestBody ClienteDTO cliente) throws ParseException {
		ResponseEntity<Object> response = null;
		Optional<ClienteDTO> clienteOpcional = this.clienteService.findOne(id);
		if (!clienteOpcional.isPresent()) {
			response = ResponseEntity.notFound().build();
		} else {
			cliente.setId(id);
			this.clienteService.save(cliente);
			response = new ResponseEntity<Object>(cliente, HttpStatus.OK);
		}
		return response;
	}

	@PostMapping(value = "/crear")
	public ResponseEntity<Object> guardar(@RequestBody ClienteDTO cliente) throws ParseException {
		Optional<ClienteDTO> clienteCreado = this.clienteService.save(cliente);
		return new ResponseEntity<Object>(clienteCreado, HttpStatus.OK);
	}

	@DeleteMapping(value = "/eliminar/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable(value = "id") Long id) {
		ResponseEntity<Object> response = null;
		if (id > 0) {
			this.clienteService.delete(id);
			response = new ResponseEntity<Object>(id, HttpStatus.OK);
		} else {
			response = ResponseEntity.notFound().build();
		}
		return response;

	}

}
