package com.example.authentication.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.authentication.dto.UserDTO;
import com.example.authentication.model.entities.Usuario;
import com.example.authentication.response.RespuestaServicio;
import com.example.authentication.security.TokenService;

@RestController
public class LoginController {

	@Autowired
	private TokenService tokenService;

	@PostMapping("login")
	public ResponseEntity<RespuestaServicio<UserDTO>> login(@RequestBody Usuario usuario) {
		RespuestaServicio<UserDTO> respuesta = new RespuestaServicio<UserDTO>();
		try {
			String token = tokenService.getJWTToken(usuario.getUsuario());
			UserDTO user = new UserDTO();
			user.setUser(usuario.getUsuario());
			user.setToken(token);
			respuesta.setResponse(user);

		} catch (Exception e) {
			respuesta.setError(true);
			respuesta.setMessage("Ocurrió un error al generar el token");
			return new ResponseEntity<RespuestaServicio<UserDTO>>(respuesta, HttpStatus.EXPECTATION_FAILED);
		}
		return new ResponseEntity<RespuestaServicio<UserDTO>>(respuesta, HttpStatus.OK);
	}

}
