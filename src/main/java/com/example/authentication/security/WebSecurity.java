package com.example.authentication.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true) 
public class WebSecurity extends WebSecurityConfigurerAdapter implements WebMvcConfigurer{

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		    http.headers().frameOptions().sameOrigin()
		    .and().csrf().disable()
			.authorizeRequests()                
			.antMatchers(HttpMethod.POST, "/login").permitAll()
			.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
			 .antMatchers("/v2/api-docs", "/configuration/**", "/swagger*/**", "/webjars/**", "/csrf", "/h2-console/**", "/console/**").permitAll() 
			.anyRequest().authenticated().and()
			.addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
	}
	
	@Override
	public void addCorsMappings(CorsRegistry registry)
	{
		registry.addMapping("/**").allowedMethods("GET", "POST", "PUT", "DELETE", "HEAD").allowedOrigins("*").allowedHeaders("*");
	}
	
}
