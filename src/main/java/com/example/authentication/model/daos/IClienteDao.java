package com.example.authentication.model.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.example.authentication.model.entities.Cliente;



public interface IClienteDao extends CrudRepository<Cliente, Long> , JpaRepository<Cliente, Long>{

}
