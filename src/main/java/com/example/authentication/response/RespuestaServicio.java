package com.example.authentication.response;

import lombok.Data;

@Data
public class RespuestaServicio<T>
{

	private boolean error;

	private String message;

	private T response;

}
