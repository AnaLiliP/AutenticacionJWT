package com.example.authentication.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("app.security.auth")
public class SecurityProperties
{

	private String key;

	private String tokenExpirationTime;

	private String tokenMailExpiration;

	public String getKey()
	{
		return key;
	}

	public void setKey(String key)
	{
		this.key = key;
	}

	public String getTokenExpirationTime()
	{
		return tokenExpirationTime;
	}

	public void setTokenExpirationTime(String tokenExpirationTime)
	{
		this.tokenExpirationTime = tokenExpirationTime;
	}

	public String getTokenMailExpiration()
	{
		return tokenMailExpiration;
	}

	public void setTokenMailExpiration(String tokenMailExpiration)
	{
		this.tokenMailExpiration = tokenMailExpiration;
	}

}
