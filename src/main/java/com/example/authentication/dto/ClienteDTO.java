package com.example.authentication.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Data;

@Data
public class ClienteDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String nombre;
	private String apellido;
	private String email;
	private String fechaNacimiento;


	public void setFechaFormatoDto(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		String fechaString = dateFormat.format(date);
		this.setFechaNacimiento(fechaString);
		
	}
}
