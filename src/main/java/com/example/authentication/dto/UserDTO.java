package com.example.authentication.dto;

import lombok.Data;

@Data
public class UserDTO
{

	private String user;

	private String token;
	

}
