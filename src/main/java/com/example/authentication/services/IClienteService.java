package com.example.authentication.services;

import java.util.List;
import java.util.Optional;

import com.example.authentication.dto.ClienteDTO;


public interface IClienteService {

	public List<ClienteDTO> findAll() ;
	
	public Optional<ClienteDTO> save(ClienteDTO cliente) ;
	
	public Optional<ClienteDTO> findOne(Long id);
	
	public void delete(Long id);
	
}
