package com.example.authentication.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.authentication.dto.ClienteDTO;
import com.example.authentication.model.daos.IClienteDao;
import com.example.authentication.model.entities.Cliente;
import com.example.authentication.utils.UtilidadesCliente;

@Service
public class ClienteServiceImpl implements IClienteService{

	@Autowired
	private IClienteDao clienteDao;
	
	@Autowired
	private UtilidadesCliente utilCliente;
	
	@Override
	@Transactional(readOnly = true)
	public List<ClienteDTO> findAll() {
		List<Cliente> clientes = (List<Cliente>) clienteDao.findAll();
		return clientes.stream().map(cliente -> utilCliente.convertirEntidadEnDTO(cliente)).collect(Collectors.toList());
	}

	@Override
	@Transactional
	public Optional<ClienteDTO> save(ClienteDTO clienteDto)  {
		Cliente cliente = utilCliente.convertirEnEntidad(clienteDto);
		Cliente clCreado = clienteDao.save(cliente);
		return Optional.of(utilCliente.convertirEntidadEnDTO(clCreado));
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<ClienteDTO> findOne(Long id) {
		Cliente cliente = clienteDao.getById(id);
		return Optional.of(utilCliente.convertirEntidadEnDTO(cliente)) ;
	}
	
	
	@Override
	@Transactional
	public void delete(Long id) {
		clienteDao.deleteById(id);
	}
	
}
