package com.example.authentication.utils;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.authentication.dto.ClienteDTO;
import com.example.authentication.model.entities.Cliente;

@Component
public class UtilidadesCliente {

	 @Autowired
	 private ModelMapper modelMapper;
	 
		public ClienteDTO convertirEntidadEnDTO(Cliente cliente)  {
			ClienteDTO clienteDto = modelMapper.map(cliente, ClienteDTO.class);
		    clienteDto.setFechaFormatoDto(cliente.getFechaNacimiento());
		    return clienteDto;
		}
		
		public Cliente convertirEnEntidad(ClienteDTO clienteDto) {
			Cliente cliente = modelMapper.map(clienteDto, Cliente.class);
		    return cliente;
		}

}
