package com.example.authentication.utils;

public class Constants {

	// Spring Security
	public static final String HEADER_AUTHORIZACION_KEY = "Authorization";
	public static final String TOKEN_BEARER_PREFIX = "Bearer ";
	public static final String TOKEN_SECRET_KEY = "mySecretKey";
	
}
